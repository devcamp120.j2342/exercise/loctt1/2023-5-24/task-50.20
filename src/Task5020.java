import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task5020 {
    public static void main(String[] args) throws Exception {
        String inputString = "Devcamp";
        int [] inputArray = {1,2,3};

        boolean inputStringArray = isArray(inputString);
        boolean inputArrayArray = isArray(inputArray);

        System.out.println(inputStringArray);
        System.out.println(inputArrayArray);

        System.out.println("-----------------------------------------");

        int[] arr = {1, 2, 3, 4, 5, 6};
        int n1 = 3;
        int n2 = 6;
        
        Integer element1 = getElement(arr, n1);
        Integer element2 = getElement(arr, n2);
        
        System.out.println(element1); 
        System.out.println(element2); 

        System.out.println("-----------------------------------------");
        int [] bubbleSort= {3, 8, 7, 6, 5, -4, -3, 2, 1};
        bubbleSort(bubbleSort);
        
        System.out.print("Output: ");
        for (int num : bubbleSort) {
            System.out.print(num + " ");
        }

        System.out.println("-----------------------------------------");
        int[] mangTraVeViTriN  = {1, 2, 3, 4, 5, 6};
        int target1 = 3;
        int target2 = 7;
        
        int index1 = findIndex(arr, target1);
        int index2 = findIndex(arr, target2);
        
        System.out.println("Output: " + index1);
        System.out.println("Output: " + index2);

        System.out.println("-----------------------------------------");

        int[] array1 = {1, 2, 3};
        int[] array2 = {4, 5, 6};
        
        int[] result = concatenateArrays(array1, array2);
        System.out.println(Arrays.toString(result));

        System.out.println("-----------------------------------------");
        Object[] inputArr = {Double.NaN, 0, 15, false, -22, "html", "develop", 47, null};

        Object[] outputArr = filterArray(inputArr);
        
        String joinedString = String.join(", ", convertToStringArray(outputArr));
        System.out.println(joinedString);

        System.out.println("-----------------------------------------");
        int[] arrRemove = {2, 5, 9, 6};
        int n = 5;

        int[] resultRemove = removeElement(arrRemove, n);
        System.out.println(Arrays.toString(resultRemove));

        System.out.println("-----------------------------------------");
        int[] arrRandom = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        int randomElement1 = getRandomElement(arrRandom);
        System.out.println(randomElement1);

        int randomElement2 = getRandomElement(arrRandom);
        System.out.println(randomElement2);

        int randomElement3 = getRandomElement(arrRandom);
        System.out.println(randomElement3);


        System.out.println("-----------------------------------------");
        int x = 6;
        int y = 0;
        int[] arrayCreate1 = createArray(x, y);
        System.out.println(Arrays.toString(arrayCreate1));

        x = 4;
        y = 11;
        int[] arrayCreate2 = createArray(x, y);
        System.out.println(Arrays.toString(arrayCreate2));

        System.out.println("-----------------------------------------");

        int x1 = 1, y1 = 4;
        int x2 = -6, y2 = 4;
        
        int[] result1 = createArray2(x1, y1);
        int[] result2 = createArray2(x2, y2);
        
        System.out.println(Arrays.toString(result1));
        System.out.println(Arrays.toString(result2));
    }

    //phương thức kiểm tra đúng sai
    public static boolean isArray(Object obj) {
        return obj.getClass().isArray();
    }

    //phương thức trả về phần tử n của mảng
    public static Integer getElement(int [] arr, int n) {
        if( n >= 0 && n < arr.length){
            return arr[n];
        }
        else{
            return null;
        }
       
    }

    //phương thức trả về mảng sắp xếp theo thứ tự tăng dần (bubble sort)
    public static void bubbleSort(int [] arr) {
        int n = arr.length;
        for(int i = 0; i < n - 1; i++){
            for(int j = 0; j < n - i -1; j++){
                if(arr[j] > arr[j+1]){
                    //hoán đổi giá trị của 2 phần tử
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;

                }
            }
        }
    }

    //phương thức trả về phần tử thứ n trong bảng
    public static int findIndex(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) {
                return i;
            }
        }
        return -1;
    }

    //phương thức nối 2 mảng lại với nhau tạo thành 1 mảng mới 
    public static int[] concatenateArrays(int[] array1, int[] array2) {
        int[] result = new int[array1.length + array2.length];
        
        System.arraycopy(array1, 0, result,0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        
        return result;
    }

    //phương thức trả về số hoặc chuỗi 
    public static Object[] filterArray(Object[] inputArray) {
        List<Object> filteredList = new ArrayList<>();
        
        for (Object item : inputArray) {
            if (item instanceof Number || item instanceof String) {
                filteredList.add(item);
            }
        }
        
        return filteredList.toArray();
    }
    
    public static String[] convertToStringArray(Object[] array) {
        String[] stringArray = new String[array.length];
        
        for (int i = 0; i < array.length; i++) {
            stringArray[i] = String.valueOf(array[i]);
        }
        
        return stringArray;
    }


    //phương thức bỏ phần tử có giá trị bằng n
    public static int[] removeElement(int[] arr, int n) {
        List<Integer> list = new ArrayList<>();

        for (int i : arr) {
            if (i != n) {
                list.add(i);
            }
        }

        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }

        return result;
    }

    //phương thức lấy random một phần tử bất kỳ trong mảng
    public static int getRandomElement(int[] arr) {
        Random random = new Random();
        int randomIndex = random.nextInt(arr.length);
        return arr[randomIndex];
    }

    //phương thức tạo ra một phần x có giá trị y
    public static int[] createArray(int x, int y) {
        int[] array = new int[x];
        Arrays.fill(array, y);
        return array;
    }

    //phương thức tạo ra mảng gồm y là số liên tiêps bắt đầu từ giá trị y
    public static int[] createArray2(int x, int y) {
        int[] array = new int[y];
        for (int i = 0; i < y; i++) {
            array[i] = x + i;
        }
        return array;
    }
}
